<?php 
require_once("funcoes.php");

extract($_REQUEST);

$postdata = file_get_contents("php://input");
$aDados = json_decode($postdata);



// EXTRAI OS DADOS 
$tipo_request = $_SERVER['REQUEST_METHOD'];


if ($location) {	
	$location = str_replace('/app', '', $location);

	$aux = explode('/', $location);
	$codigo = end($aux);
}

if (isset($_FILES) && array_key_exists("file", $_FILES)) {
	$arquivo = $_FILES['file']['name'];
	$arquivo = basename($arquivo);
	copy($_FILES['file']['tmp_name'], SERVER_PATH . "temp/" . $arquivo);
	$_SESSION['anexos'][] = $_FILES;
}

unset($_FILES);

if ($p == 'removerAnexos') {
	unset($_FILES);
	unset($_SESSION['anexos']);	
	return;
}


if (isset($classe)) {
	// Instanciando
	$classe = new $classe();
	
	// Escolhe o tipo do GET que vem pelo protocolo lá do app.js ($http.get, $http.post)
	switch ($tipo_request) {

		case 'GET':
			$classe->$p($codigo, $pesquisa);
			break;
		case 'POST':			
			$classe->$p($codigo, $aDados, $_SESSION['anexos']);
			unset($_SESSION['anexos']);
			break;
		case 'PUT':
			$classe->$p($codigo, $aDados);	
			break;
		default:
			echo "Sem classe";
			break;
	}

}
