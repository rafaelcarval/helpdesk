<?php

require_once('classes/dao.php');
require_once('classes/cliente.php');
require_once('classes/session.php');
require_once('classes/generic.php');
require_once('classes/ticket.php');
require_once('classes/usuario.php');
require_once('classes/grafico.php');
require_once('classes/cron.php');
require_once("downloadfileclass.inc");

error_reporting(0);

session_start();

if ($_SERVER["SERVER_ADDR"] != "192.168.10.20") { 
    define("bPublicado", true);  
    define("SERVER_PATH", "/mnt/dados/helpdesk/");    
} else {    
    define("bPublicado", false);   
    define("SERVER_PATH", "/var/www/html/dados/safetydocshelpdesk/");
}


function replaceAcentos($str) {
  $a = array('À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ð', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'ß', 'à', 'á', 'â', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ', 'Ā', 'ā', 'Ă', 'ă', 'Ą', 'ą', 'Ć', 'ć', 'Ĉ', 'ĉ', 'Ċ', 'ċ', 'Č', 'č', 'Ď', 'ď', 'Đ', 'đ', 'Ē', 'ē', 'Ĕ', 'ĕ', 'Ė', 'ė', 'Ę', 'ę', 'Ě', 'ě', 'Ĝ', 'ĝ', 'Ğ', 'ğ', 'Ġ', 'ġ', 'Ģ', 'ģ', 'Ĥ', 'ĥ', 'Ħ', 'ħ', 'Ĩ', 'ĩ', 'Ī', 'ī', 'Ĭ', 'ĭ', 'Į', 'į', 'İ', 'ı', 'Ĳ', 'ĳ', 'Ĵ', 'ĵ', 'Ķ', 'ķ', 'Ĺ', 'ĺ', 'Ļ', 'ļ', 'Ľ', 'ľ', 'Ŀ', 'ŀ', 'Ł', 'ł', 'Ń', 'ń', 'Ņ', 'ņ', 'Ň', 'ň', 'ŉ', 'Ō', 'ō', 'Ŏ', 'ŏ', 'Ő', 'ő', 'Œ', 'œ', 'Ŕ', 'ŕ', 'Ŗ', 'ŗ', 'Ř', 'ř', 'Ś', 'ś', 'Ŝ', 'ŝ', 'Ş', 'ş', 'Š', 'š', 'Ţ', 'ţ', 'Ť', 'ť', 'Ŧ', 'ŧ', 'Ũ', 'ũ', 'Ū', 'ū', 'Ŭ', 'ŭ', 'Ů', 'ů', 'Ű', 'ű', 'Ų', 'ų', 'Ŵ', 'ŵ', 'Ŷ', 'ŷ', 'Ÿ', 'Ź', 'ź', 'Ż', 'ż', 'Ž', 'ž', 'ſ', 'ƒ', 'Ơ', 'ơ', 'Ư', 'ư', 'Ǎ', 'ǎ', 'Ǐ', 'ǐ', 'Ǒ', 'ǒ', 'Ǔ', 'ǔ', 'Ǖ', 'ǖ', 'Ǘ', 'ǘ', 'Ǚ', 'ǚ', 'Ǜ', 'ǜ', 'Ǻ', 'ǻ', 'Ǽ', 'ǽ', 'Ǿ', 'ǿ');
  $b = array('A', 'A', 'A', 'A', 'A', 'A', 'AE', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'D', 'N', 'O', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 's', 'a', 'a', 'a', 'a', 'a', 'a', 'ae', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y', 'A', 'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 'c', 'C', 'c', 'D', 'd', 'D', 'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 'g', 'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'IJ', 'ij', 'J', 'j', 'K', 'k', 'L', 'l', 'L', 'l', 'L', 'l', 'L', 'l', 'l', 'l', 'N', 'n', 'N', 'n', 'N', 'n', 'n', 'O', 'o', 'O', 'o', 'O', 'o', 'OE', 'oe', 'R', 'r', 'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'S', 's', 'T', 't', 'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'W', 'w', 'Y', 'y', 'Y', 'Z', 'z', 'Z', 'z', 'Z', 'z', 's', 'f', 'O', 'o', 'U', 'u', 'A', 'a', 'I', 'i', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'A', 'a', 'AE', 'ae', 'O', 'o');
  return str_replace($a, $b, $str);
}

function organizaAnexos($sAnexos){
    
    $aAnexos = explode(',', $sAnexos);
    $aAnexos = array_filter($aAnexos, 'strlen');

    foreach ($aAnexos as $key => $anexo) {
        $aux = explode('/', $anexo);
        $aResult[$key]['pasta'] = $aux[0];
        $aResult[$key]['nome_original'] = $aux[1];
        $aResult[$key]['label'] = substr($aux[1], 14);
    }

    return (object)$aResult;
}

function dateToHtml($date, $com_hora = true, $apenas_hora = false){
    
    $auxData = explode(' ', $date);
    $aData = $auxData[0];   
    $aHora = $auxData[1]; 

    $finalDate = implode('/', array_reverse(explode('-', $aData)));
    $finalDate .= (isset($auxData[1]) && $com_hora) ? " " . substr($auxData[1], 0, 5) : '';
    $finalHour =  substr($auxData[1], 0, 5);

    if ($apenas_hora == true) {
        return $finalHour;
    } else {
        return $finalDate;
    }
}

function dateToSQL($date){
    $auxData = explode('/', $date);

    return $auxData[2] . '-' . $auxData[1] . '-' . $auxData[0];
}

function ArrayEncode($array) {
    if (is_object($array)) {
        $array = (array) $array;
        $obj = true;
    }
    foreach ($array as $k => $v) {
        if (is_array($v))
            $array[$k] = ArrayEncode($v);
        else
            $array[$k] = _encodeUTF8String($v);
    }
    if (isset($obj) && $obj === true)
        $array = (object) $array;
    return $array;
}

function ArrayDecode($array) {
    if (is_object($array)) {
        $array = (array) $array;
        $obj = true;
    }
    foreach ($array as $k => $v) {
        if (is_array($v))
            $array[$k] = ArrayDecode($v);
        else
            $array[$k] = _decodeUTF8String($v);
    }
    if (isset($obj) && $obj === true)
        $array = (object) $array;
    return $array;
}

function buildSet ($aDados) {
    $sSet = 'SET ';
    $i = 1;

    foreach ($aDados as $key => $value) {
        foreach ($value as $key2 => $value2) {            

            // var_dump($value2);
            $value2 = str_replace("'", "", $value2);
            
            $sSet .= $key2 . " = '" . $value2 . "' ";

                if ($i < sizeof((array)$value)) {
                    $sSet .= ", ";
                }
                
            $i++; 
        }
    }
    return $sSet;
}

function buildPesquisa ($aDados) {

    $sSet = 'WHERE ';
    $i = 1;

    foreach ($aDados as $key => $value) {
        foreach ($value as $key2 => $value2) {
            
            // Groselha doc?sima - Lucas 25/08/2017
            if (strstr($key2, 'data_inicial')) {
                $key2 = 'data';
                $sSinal = " >= ";
            }
            if (strstr($key2, 'data_final')) {
                $key2 = 'data';
                $sSinal = " <= ";
            }

            if (strstr($key2, 'data')) {
                $value2 = dateToSQL($value2);
            }

            $sSinal =  $sSinal == '' ? ' = ' : $sSinal;

            $sSet .= $key2 . $sSinal . "'" . _decodeUTF8String($value2) . "' ";

                if ($i < sizeof((array)$value)) {
                    $sSet .= "AND ";
                }
                
            $i++; 
        }
    }
    return $sSet;
}


function _getCharStrEncoded($str) {
    return mb_detect_encoding($str . 'x', 'UTF-8, ISO-8859-1');
}

function _decodeUTF8String($s) {
    $chr = _getCharStrEncoded($s);
    if ($chr == 'UTF-8') {
        $s = utf8_decode($s);
    }
    return $s;
}

function _encodeUTF8String($s) {
    $chr = _getCharStrEncoded($s);
    if ($chr != 'UTF-8') {
        $s = utf8_encode($s);
    }
    return $s;
}


function sendEmail($codticket, $nome_cliente, $email_cliente, $empresa_cliente, $mensagem, $anexo="", $tipo){

    $aConfiguracoes = Generic::getConfiguracoes('array');

    $nome_empresa = explode('|', $configuracoes['nome']);

    // $link = mysqli_connect('192.168.10.20','root','proxy','db_safetydocs_helpdesk');
    $link = mysqli_connect('localhost','db_suporteproxy','JhHF5QK7qT032vTQ','db_safetydocs_helpdesk');
    mysqli_select_db($link, "db_safetydocs");


    $sQuery = "SELECT assunto, msg FROM emails_anuncios WHERE tipo = '" . $tipo . "'";

    $oStmt = mysqli_query($link, $sQuery) or mysqli_error($sQuery); 
    
    $aResult = array();             

    while($oResult = mysqli_fetch_assoc($oStmt)){               
        array_push($aResult, ArrayEncode($oResult));
    }


    $sAssuntoEmail = $aResult[0]['assunto'];
    $sCorpoEmail = base64_decode($aResult[0]['msg']);
    


    $aTags = array(
                "#{nome_do_cliente}",
                "#{codigo_ticket}",                
                "#{endereco_site}",                                            
                "#{mensagem_resposta}",
            );

    $aReplace = array(
                    $nome_cliente,
                    "<a href='" . $aConfiguracoes['link_sistema'] . "/#/app/VerTicketCliente/" . md5($codticket) . "'> Visualizar Ticket</a>",
                    "<a href='" . $aConfiguracoes['link_sistema'] . "'>" . $nome_empresa[1] . "</a>",
                    nl2br($mensagem)
                );

    $sEmailFinal = str_replace($aTags, $aReplace, $sCorpoEmail);
    

    $categoria = 'safetydocs-helpdesk-' . $codticket;

    _emailSendGrid(
                    array("noreply@proxysystems.com.br", $nome_empresa[1]), 
                    array($email_cliente, 'talita.bidim@safetydocs.com.br'),
                    $sAssuntoEmail . " - Ticket Nº " . $codticket,
                    $sEmailFinal,
                    $anexo="", 
                    $bcc = false, 
                    $auth=true, 
                    $categoria
                );



    mysqli_select_db($link, "db_safetydocs_helpdesk");

}

function _emailSendGrid($from, $to, $subject, $msg, $anexo="", $bcc = false, $auth=true, $categoria = false) {    

    // die($from. print_r($to). $subject. $msg. $anexo. $bcc . $auth. $categoria );
    
    $subject = _encodeUTF8String($subject);
    $msg = _encodeUTF8String($msg);

    if (bPublicado) {
        include('smtpapi-php-sendgrid/app/swift_mailer.php');
    } else {
        // include('/var/www/html/proxymind_modulos/smtpapi-php-sendgrid/app/swift_mailer.php');
    }

}

function numberToMonth($number){
    switch ($number) {
        case '1':
            return "Janeiro";
            break;
        case '2':
            return "Fevereiro";
            break;
        case '3':
            return "Março";
            break;
        case '4':
            return "Abril";
            break;
        case '5':
            return "Maio";
            break;
        case '6':
            return "Junho";
            break;
        case '7':
            return "Julho";
            break;
        case '8':
            return "Agosto";
            break;
        case '9':
            return "Setembro";
            break;
        case '10':
            return "Outubro";
            break;
        case '11':
            return "Novembro";
            break;
        case '12':
            return "Dezembro";
            break;
        default:
            // die($number . "<<");
            return "Inválido";
            break;
    }

}

function slug($string){

    $accents = array(
        'Š' => 'S',
        'š' => 's',
        'Đ' => 'Dj',
        'đ' => 'dj',
        'Ž' => 'Z',
        'ž' => 'z',
        'Č' => 'C',
        'č' => 'c',
        'Ć' => 'C',
        'ć' => 'c',
        'À' => 'A',
        'Á' => 'A',
        'Â' => 'A',
        'Ã' => 'A',
        'Ä' => 'A',
        'Å' => 'A',
        'Æ' => 'A',
        'Ç' => 'C',
        'È' => 'E',
        'É' => 'E',
        'Ê' => 'E',
        'Ë' => 'E',
        'Ì' => 'I',
        'Í' => 'I',
        'Î' => 'I',
        'Ï' => 'I',
        'Ñ' => 'N',
        'Ò' => 'O',
        'Ó' => 'O',
        'Ô' => 'O',
        'Õ' => 'O',
        'Ö' => 'O',
        'Ø' => 'O',
        'Ù' => 'U',
        'Ú' => 'U',
        'Û' => 'U',
        'Ü' => 'U',
        'Ý' => 'Y',
        'Þ' => 'B',
        'ß' => 'Ss',
        'à' => 'a',
        'á' => 'a',
        'â' => 'a',
        'ã' => 'a',
        'ä' => 'a',
        'å' => 'a',
        'æ' => 'a',
        'ç' => 'c',
        'è' => 'e',
        'é' => 'e',
        'ê' => 'e',
        'ë' => 'e',
        'ì' => 'i',
        'í' => 'i',
        'î' => 'i',
        'ï' => 'i',
        'ð' => 'o',
        'ñ' => 'n',
        'ò' => 'o',
        'ó' => 'o',
        'ô' => 'o',
        'õ' => 'o',
        'ö' => 'o',
        'ø' => 'o',
        'ù' => 'u',
        'ú' => 'u',
        'û' => 'u',
        'ý' => 'y',
        'ý' => 'y',
        'þ' => 'b',
        'ÿ' => 'y',
        'Ŕ' => 'R',
        'ŕ' => 'r',
        '/' => '-',
        ' ' => '-',
        '.' => '-',
    );

    $string = strtr($string, $accents);

    $string = preg_replace('/-{2,}/', '-', $string);
    $string = strtolower($string);

    return $string;
}

