<?php

require_once("downloadfileclass.inc");


if ($_SERVER["SERVER_ADDR"] != "192.168.10.20") { 
    define("bPublicado", true);  
    define("SERVER_PATH", "/mnt/dados/helpdesk/");    
} else {    
    define("bPublicado", false);   
    define("SERVER_PATH", "/var/www/html/dados/");
}

$sDir = SERVER_PATH . "tickets_anexos/" . $_GET['p'] . "/" . $_GET['f'];

$oDownloadfile = new DOWNLOADFILE($sDir, '', "attachment");

if (!$oDownloadfile->df_download()){
	$html .= '<html>';
	$html .= '<head>';
	$html .= '<title>Falha no download</title>';
	$html .= '</head>';
	$html .= '<body style="font:normal 12px Tahoma; color:#000; background-color:#FAFAFA; margin:10px;">';
	$html .= '<h4>Falha no download</h4>';
	$html .= '<p>Desculpe, houve um erro no download deste item. Caso o problema persista, entre em contato conosco.</p>';
	$html .= '<p style="font-weight:bold; color:#036;">Arquivo: ' . $sDir . '</p>';
	$html .= '</body>';
	$html .= '</html>';
	
	echo $html;
}

exit;
?>