<?php
	
class Generic extends Dao {


	public $sTable = 'clientes';
	public $sFields = '';


	public function getConfiguracoes($return){

		// Confere se j� est� na sess�o e devolve.
		// se passar pelo updateConfiguracoes, joga o atualiza_configuracoes pra true e for? ele a buscar de novo.
		if (Session::get('configuracoes_sistema') && !Session::get('atualiza_configuracoes')) {
			if ($return == 'array') {			
				return Session::get('configuracoes_sistema');
			} else {
				echo json_encode(Session::get('configuracoes_sistema'));
			}
			return;
		}

		// Se n�o, busca e seta na sess�o
		$sTable = "configuracoes";	

		$Dao = new Dao();
		$aEmpresa = $Dao->getData($sTable, '', $sFields, '', '');

		Session::set('configuracoes_sistema', $aEmpresa);

		if ($return == 'array') {
			return $aEmpresa;
		} else {
			echo json_encode($aEmpresa);
		}
	}

	public function getCategorias(){

		$sTable = "categorias";
		$sWhere = "WHERE ativo = 'S' ORDER BY ativo ASC";

		$aCategorias = $this->getData($sTable, $sWhere, $sFields, '', 'array');
        
		echo json_encode($aCategorias);
	}

	public function getModelos(){

		$sTable = "modelos";
		$sWhere = " WHERE ativo = 'S'";

		$aModelos = $this->getData($sTable, $sWhere, $sFields, '', 'array');
        
		echo json_encode($aModelos);
	}


	public function getRecursosByCodigo($codigos){

		$sTable = "recursos";
		$sWhere = "WHERE codrecurso IN (" . implode(',', $codigos) . ") ORDER BY ordem ASC";

		$aRecursos = $this->getData($sTable, $sWhere, '', '', 'array');

		return $aRecursos;
	}

	public function getTodosRecursos(){

		$sTable = "recursos";
		$sWhere = "ORDER BY ordem ASC";

		$aRecursos = $this->getData($sTable, $sWhere, $this->sFields, '', 'array');
        
		echo json_encode($aRecursos);
	}
    
    // MODELOS
    public function insertModelo($q, $aDados){    

    	$sTable = 'modelos';
		$sSet = buildSet($aDados);		

		$return = $this->insertData($sTable, $sSet);
		

		echo json_encode($return);
	}

	public function updateModelo($q, $aDados){

        $sTable = 'modelos';
        
		$sSet = buildSet($aDados);	
        
		$sWhere = "WHERE codmodelo = '" . $aDados->oModelo->codmodelo . "' ";

		$return = $this->updateData($sTable, $sWhere, $sSet);
	}

	public function deleteModelo($q, $aDados){

		$sTable = 'modelos';

		$sWhere = "WHERE codmodelo = '" . $aDados->oModelo->codmodelo . "' ";
		$this->deleteData($sTable, $sWhere);			

		echo json_encode(array('msg' => 'true'));
	}			
	// FIM MODELOS


	// CATEGORIAS
	public function insertCategoria($q, $aDados){    

    	$sTable = 'categorias';

		$sSet = "SET descricao = '" . utf8_decode($aDados->oCategoria->descricao) . "' ";

		$return = $this->insertData($sTable, $sSet, '');
	
		echo json_encode($return);
	}

	public function updateCategoria($q, $aDados){

        $sTable = 'categorias';
        
		$sSet = buildSet($aDados);	
        
		$sWhere = "WHERE codcategoria = '" . $aDados->oCategoria->codcategoria . "' ";

		$return = $this->updateData($sTable, $sWhere, $sSet);
	}

	public function deleteCategoria($q, $aDados){

		$sTable = 'categorias';

		$sWhere = "WHERE codcategoria = '" . $aDados->oCategoria->codcategoria . "' ";
		$this->deleteData($sTable, $sWhere);			

		echo json_encode(array('msg' => 'true'));
	}
	// FIM CATEGORIAS

	// CONFIGURA��ES GERAIS
	public function updateConfiguracoes($q, $aDados){

        $sTable = 'configuracoes';
        
		$sSet = buildSet($aDados);	
        
		$sWhere = "WHERE codconfig = '" . $aDados->oConfiguracao->codconfig . "' ";

		Session::set('atualiza_configuracoes', true);

		$return = $this->updateData($sTable, $sWhere, $sSet, '');
	}

	public function insertLog($codticket, $status_ticket, $tipo, $tipo_usuario, $nome_usuario, $aDados){    

    	$sTable = 'tickets_log';

    	$sSet = "SET 
    				codticket = '" . $codticket . "',
    				status_ticket = '" . utf8_decode($status_ticket) . "',
    				tipo = '" . $tipo . "',
    				tipo_usuario = '" . $tipo_usuario . "',
    				nome_usuario = '" . _decodeUTF8String($nome_usuario) . "',
    				log = '" . json_encode($aDados) . "',
    				ip = '" . $_SERVER['REMOTE_ADDR'] . "',
    				data = NOW() ";

		$this->insertData($sTable, $sSet, '');
		
	}
	

}

?>