<?php
	
class Usuario extends Dao {

	public $sTable = 'usuarios';
	public $sFields = '';
	public $sAtivo = "AND ativo = 'S'";


	public function verificaUser(){

		$userSession = Session::get('user');

		if (isset($userSession)) {
			
			$ticket = new Ticket();
			$userSession['notificacoes']  = $ticket->getNotificacoes($userSession['codusuario']);

			echo json_encode($userSession);
		} else {
			echo json_encode(array('msg' => 'false'));
		}
	}

	public function getUsuarioLogin($codigo, $aDados){
				
		$sEmailMD5 = md5($aDados->oUser->email);
				
		$sWhere = "WHERE MD5(email) = '" . $sEmailMD5 . "' AND ativo <> 'N'";

		$aUser = $this->getData($this->sTable, $sWhere, $this->sFields, '', '');
		

		
		if ($aUser['senha'] !== $aDados->oUser->senha) {
			echo json_encode(array('msg' => 'false'));
			return;
		}

		if (count($aUser) <= 0) {
			echo json_encode(array('msg' => 'false'));
			return;
		}

		
		$aUser['recursos'] = json_decode($aUser['recursos']);
		//$aUser['notificacoes'] = Ticket::getNotificacoes($aUser['codusuario']);;
		
		$aRecursos = array();
		
		foreach ($aUser['recursos'] as $key => $value) {
			if ($value !== false) {				
				array_push($aRecursos, $key);
			}
		}
		
		$gen = new Generic();

		$aUser['menu'] = $gen->getRecursosByCodigo($aRecursos);
		$aUser['tipo'] = 'usuario';


		if (isset($aUser)) {
			Session::set('user', $aUser);
		}

		echo json_encode($aUser);
	}

	public function getUsuarios(){

		$sWhere = "";

		$aUsers = $this->getData($this->sTable, $sWhere, $sFields, '', 'array');

		foreach ($aUsers as $key => $value) {
			$aUsers[$key]['recursos'] = json_decode($aUsers[$key]['recursos']);
		}

		echo json_encode($aUsers);
	}
		
	public function insertUsuario($codigo, $aDados){

        $sTable = 'usuarios';

        // S� pode haver um usu�rio com auto atribui��o (Highlander). Se vier S, joga todo mundo pra N e atualiza o atual pra S
        if ($aDados->oUsuario->auto_atribuir == 'S') {        
			$return = $this->updateData($sTable, '', "SET auto_atribuir = 'N'");
        }

		$sSet = "SET
					nome = '" . utf8_decode($aDados->oUsuario->nome) . "',
					email = '" . $aDados->oUsuario->email . "' ,
					assinatura = '<br /><br />" . utf8_decode($aDados->oUsuario->assinatura) . "',					
					tipo_usuario = '" . $aDados->oUsuario->tipo_usuario . "',
					auto_atribuir = '" . $aDados->oUsuario->auto_atribuir . "',
				 	tema = '" . $aDados->oUsuario->tema . "' ";
		
		if (($aDados->oUsuario->nova_senha == $aDados->oUsuario->confirmacao_nova_senha) && ($aDados->oUsuario->nova_senha != '' && $aDados->oUsuario->confirmacao_nova_senha != '')) {
			$sSet .= ", senha = '" . utf8_decode($aDados->oUsuario->nova_senha) . "'";
		}

		$recursos_json = json_encode($aDados->oUsuario->recursos);

		$sSet .= ", recursos = '$recursos_json' ";

		$return = $this->insertData($sTable, $sSet);			

	}

	public function updateUsuario($codigo, $aDados){

        $sTable = 'usuarios';

        // S?pode haver um usu?io com auto atribui?o. Se vier S, joga todo mundo pra N e atualiza o atual pra S
        if ($aDados->oUsuario->auto_atribuir == 'S') {        
			$return = $this->updateData($sTable, '', "SET auto_atribuir = 'N'");
        }
        
		$sSet = "SET
					nome = '" . utf8_decode($aDados->oUsuario->nome) . "',
					email = '" . $aDados->oUsuario->email . "' ,					
					assinatura = '" . utf8_decode($aDados->oUsuario->assinatura) . "',
					ativo = '" . $aDados->oUsuario->ativo . "',
					tipo_usuario = '" . $aDados->oUsuario->tipo_usuario . "',
					auto_atribuir = '" . $aDados->oUsuario->auto_atribuir . "',
				 	tema = '" . $aDados->oUsuario->tema . "' ";
		
		if (($aDados->oUsuario->nova_senha == $aDados->oUsuario->confirmacao_nova_senha) && ($aDados->oUsuario->nova_senha != '' && $aDados->oUsuario->confirmacao_nova_senha != '')) {
			$sSet .= ", senha = '" . utf8_decode($aDados->oUsuario->nova_senha) . "'";
		}

		$recursos_json = json_encode($aDados->oUsuario->recursos);

		$sSet .= ", recursos = '$recursos_json' ";

		$sWhere = "WHERE codusuario = '" . $aDados->oUsuario->codusuario . "' ";

		$return = $this->updateData($sTable, $sWhere, $sSet);
	}


	public function getUsuario($codigo){

		$sWhere = "WHERE codusuario = '$codigo'";

		$aUser = $this->getData($this->sTable, $sWhere, $sFields);

		$aUser['recursos'] = json_decode($aUser['recursos']);
       
		echo json_encode($aUser);
	}
} 
?>