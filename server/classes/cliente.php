<?php
	
class Cliente extends Dao {

	public $sTable = 'clientes';
	public $sFields = '';
	public $sAtivo = "AND ativo = 'S'";


	public function verificaCliente(){

		$clienteSession = Session::get('user');

		if (isset($clienteSession)) {
			echo json_encode($clienteSession);
		} else {			
			echo json_encode(array('msg' => 'false'));
		}
	}


	public function getClienteSessionRedirect($codigo, $dados){
		// $dados é o email do cliente em md5
		//testar com https://sistema.safetydocs.com.br/helpdesk/server/redirect.php?p=getClienteSessionRedirect&classe=cliente&pesquisa=6a5386b0cdcf71f2cf8673fc2342f6cb
		$sEmailMD5 = $dados;
				
				
		$sWhere = "WHERE MD5(email) = '" . $sEmailMD5 . "' AND ativo <> 'N'";

		$aCliente = $this->getData($this->sTable, $sWhere, $this->sFields, '', '');		


		if (count($aCliente) <= 0) {
			die('acesso negado');			
		}	

		$aCliente['recursos'] = json_decode($aCliente['recursos']);

		$aRecursos = array();
		
		foreach ($aCliente['recursos'] as $key => $value) {
			if ($value !== false) {				
				array_push($aRecursos, $key);
			}
		}

		$gen = new Generic();

		$aCliente['menu'] = $gen->getRecursosByCodigo($aRecursos);
		$aCliente['tipo'] = 'cliente';

		if (isset($aCliente)) {
			Session::set('user', $aCliente);
		}
		
		

		//echo json_encode($aCliente);		
		
		header("Location: https://sistema.safetydocs.com.br/helpdesk/#/app/TicketsCliente");
		exit;
		
		
	}


	public function getClienteLogin($codigo, $aDados){

		$sEmailMD5 = md5($aDados->oCliente->email);
				
		$sWhere = "WHERE MD5(email) = '" . $sEmailMD5 . "' AND ativo <> 'N'";

		$aCliente = $this->getData($this->sTable, $sWhere, $this->sFields, '', '');		


		if (count($aCliente) <= 0) {
			echo json_encode(array('msg' => 'false'));
			return;
		}	

		$aCliente['recursos'] = json_decode($aCliente['recursos']);

		$aRecursos = array();
		
		foreach ($aCliente['recursos'] as $key => $value) {
			if ($value !== false) {				
				array_push($aRecursos, $key);
			}
		}

		$gen = new Generic();

		$aCliente['menu'] = $gen->getRecursosByCodigo($aRecursos);
		$aCliente['tipo'] = 'cliente';

		if (isset($aCliente)) {
			Session::set('user', $aCliente);
		}

		echo json_encode($aCliente);
	}


	public function insertCliente($codigo, $aDados){

        $sTable = 'clientes';

        $verificaEmailExistente = $this->getData($sTable, "WHERE email = '" . $aDados->oCliente->email . "'", '', '', 'array');

        if (count($verificaEmailExistente) > 0) {
        	echo json_encode(array('msg' => 'E-mail existente.'));
        	return;
        }

		$sSet = "SET
					nome = '" . utf8_decode($aDados->oCliente->nome) . "',
					email = '" . $aDados->oCliente->email . "' ,											
					empresa = '" . utf8_decode($aDados->oCliente->empresa) . "' ,											
					tel = '" . $aDados->oCliente->tel . "' ,											
					dtcadastro = NOW(),
					cargo = '" . utf8_decode($aDados->oCliente->cargo) . "'";

		$this->insertData($sTable, $sSet, '');

	}


	public function getClientes($codigo, $pesquisa){		

		$sWhere = $pesquisa !== 'todos' ? "WHERE CONCAT(nome, ' ' ,email, ' ' ,empresa) LIKE '%" . $pesquisa . "%'" : "";
		$sWhere .= "ORDER BY empresa, nome ASC";

		$aClientes = $this->getData($this->sTable, $sWhere, $sFields, '', 'array');
        
		echo json_encode($aClientes);
	}


	// Ultimos tickets do cliente
	public function getUltimosTicketsCliente($cliente_empresa){		

		$sTable = "tickets t INNER JOIN clientes c ON t.codcliente = c.codcliente";
		$sFields = "t.assunto";

		$sWhere = "WHERE c.empresa = '" . utf8_decode($cliente_empresa) . "' ORDER BY t.codticket DESC LIMIT 10";

		$aUltimosTickets = $this->getData($sTable, $sWhere, $sFields, '', 'array');
        
		return $aUltimosTickets;
	}
		
	public function getCliente($q, $aDados){

		$sWhere = "WHERE email = '" . $aDados->oCliente->email . "' ";

		$aCliente = $this->getData($this->sTable, $sWhere, $this->sFields, '', 'array');

		echo json_encode($aCliente);
	}	

	public function getClienteEditar($codigo){

		$sWhere = "WHERE codcliente = '" . $codigo . "' ";

		$aCliente = $this->getData($this->sTable, $sWhere, $this->sFields, '', '');

		$aCliente['ultimos_tickets'] = self::getUltimosTicketsCliente($codigo);

		echo json_encode($aCliente);
	}
   
   	public function updateCliente($codigo, $aDados){

   		$verificaEmailExistente = $this->getData($this->sTable, "WHERE email = '" . trim($aDados->oCliente->email) . "' AND codcliente <> '" . $aDados->oCliente->codcliente . "'", '', '', 'array');

        if (count($verificaEmailExistente) > 0) {
        	echo json_encode(array('msg' => 'E-mail existente.'));
        	return;
        }
        
       	$sSet = "SET
					nome = '" . utf8_decode($aDados->oCliente->nome) . "',
					email = '" . $aDados->oCliente->email . "' ,					
					empresa = '" . utf8_decode($aDados->oCliente->empresa) . "',
					tel = '" . $aDados->oCliente->tel . "',
					cargo = '" . utf8_decode($aDados->oCliente->cargo) . "',
					nota = '" . utf8_decode($aDados->oCliente->nota) . "',
					contato_empresa = '" . utf8_decode($aDados->oCliente->contato_empresa) . "' ";				 	

		$sWhere = "WHERE codcliente = '" . $aDados->oCliente->codcliente . "' ";

		$return = $this->updateData($this->sTable, $sWhere, $sSet, '');
	}
}

?>