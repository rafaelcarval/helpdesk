<?php

// Criar aqui os crons que ir�o expirar os tickets de 1 dia pro outro e transfer�-los de responsabilidade PROXY - SAFETY DOCS
class Cron extends Dao {

	public function expiraTickets($codigo, $q){

		// PEGA TICKETS COM �LTIMA RESPOSTA VINDA DE UM ATENDENTE H� MAIS DE 5 DIAS
		$sFields = "t.codticket, c.codcliente, c.nome, c.empresa, c.email, MD5(t.codticket) AS hask_link";
										
		$sTable = "tickets t 
					INNER JOIN tickets_mensagens tm ON t.codticket = tm.codticket
					INNER JOIN clientes c ON t.codcliente = c.codcliente";

		$sWhere = "WHERE t.status = 'Aguardando Cliente'
					AND DATEDIFF(CURDATE(), tm.data) > 5
					AND t.ultimo_respondente = 'usuario'";


		$aTicketsExpira = $this->getData($sTable, $sWhere, $sFields, '', 'array');

		foreach ($aTicketsExpira as $key => $value) {
			if ($key < count($aTicketsExpira) - 1) {
				$sCodTickets .= $value['codticket'] . ", ";
			} else {
				$sCodTickets .= $value['codticket'];				
			}

			sendEmail($value['codticket'], $value['nome'], $value['email'], $value['empresa'], '', $anexo="", 'ticketExpirado');

			Generic::insertLog(
							'envio_automatico_expirado',
							$value['codticket'], 
							'',
							$value['codcliente'],
							$value);
		}

		$sTable = "tickets";
		$sWhere = "WHERE codticket IN (" . $sCodTickets . ")";
		$sSet = "SET status = 'Expirado'";

		$this->updateData($sTable, $sWhere, $sSet, '');
		
	}

}

?>