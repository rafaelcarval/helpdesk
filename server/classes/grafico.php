<?php
	
class Grafico extends Dao {


	public $sTable = 'tickets';
	public $sFields = '';

	public function getGraficoCategorias(){

		$sFields = "c.cor, t.codcategoria, c.descricao, COUNT(t.codticket) AS total";
		$sTable = "tickets t
					INNER JOIN categorias c
					ON t.codcategoria = c.codcategoria";

		$sWhere .= "GROUP BY c.descricao ORDER BY COUNT(t.codticket) DESC";

		$aDadosCategorias = $this->getData($sTable, $sWhere, $sFields, '', '');
		
		foreach ($aDadosCategorias as $key => $value) {
			$aRet['backgroundColor'][$key] = $value['cor'];
			$aRet['data'][$key] = $value['total'];
			$aRet['labels'][$key] = $value['descricao'];
		}

		$aResult['data']['datasets'][0]['backgroundColor'] = $aRet['backgroundColor'];
		$aResult['data']['datasets'][0]['data'] = $aRet['data'];
		$aResult['data']['labels'] = $aRet['labels'];

		echo json_encode($aResult);
	}
	
	public function getGraficoAbertosResolvidosLine(){

		$sFields = "LEFT(t.data, 10) as dia, COUNT(*) AS total_resolvidos_no_dia";
		$sTable = "tickets t";
		$sWhere = "WHERE (t.ultima_resposta IS NOT NULL AND t.ultima_resposta <> '0000-00-00 00:00:00')
					AND DATEDIFF(t.data, t.ultima_resposta) = 0
					GROUP BY LEFT(t.data, 10)";

		$aResolvidosNoDia = $this->getData($sTable, $sWhere, $sFields, '', '');


		$resultMesesLabel = $this->getData('tickets t', 'GROUP BY LEFT(t.data, 10)', 'LEFT(t.data, 10) as dia', '', '');

		foreach ($resultMesesLabel as $key => $value) {		
			$aMesesLabel[] = $value['dia'];
		}		
	
		$aRet['data']['labels'] = $aMesesLabel;


		$sFields = "LEFT(t.data, 10) AS dia, COUNT(*) AS total_nao_resolvidos_no_dia";
		$sTable = "tickets t";
		$sWhere = "WHERE DATEDIFF(NOW(), t.data) > 0
					GROUP BY LEFT(t.data, 10)";

		$aNaoResolvidosNoDia = $this->getData($sTable, $sWhere, $sFields, '', '');



		foreach ($aResolvidosNoDia as $key => $value) {
			$aAuxTotais[] = $value['total_resolvidos_no_dia'];
			$aConcatLabels = $value['total_resolvidos_no_dia'];
			$aRet['data']['datasets'][0]['label'] = "Resolvidos no dia";			
			$aRet['data']['datasets'][0]['borderColor'] = "blue";
			$aRet['data']['datasets'][0]['fill'] = "false";
		}

		$aRet['data']['datasets'][0]['data'] = $aAuxTotais;

		foreach ($aNaoResolvidosNoDia as $key => $value) {
			$aAuxTotais[] = $value['total_nao_resolvidos_no_dia'];
			$aRet['data']['datasets'][1]['label'] = utf8_encode("N�o resolvidos no mesmo dia");			
			$aRet['data']['datasets'][1]['borderColor'] = "red";
			$aRet['data']['datasets'][1]['fill'] = "false";
		}
		
		$aRet['data']['datasets'][1]['data'] = $aAuxTotais;


		echo json_encode($aRet);
	}

}

?>