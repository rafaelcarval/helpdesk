<?php

class Ticket extends Dao {

	public $sTable = 'tickets';
	public $sFields = '';
	public $sAtivo = "AND ativo = 'S'";


	public function getTickets($q, $aDados){

		if (count($aDados->oPesquisaTicket) > 0) {	
			$aWherePesquisa[] = $aDados->oPesquisaTicket->codusuario != '' ? "t.codusuario = " . $aDados->oPesquisaTicket->codusuario . " " : ''; 
			$aWherePesquisa[] = $aDados->oPesquisaTicket->cliente != '' ? "CONCAT(c.empresa, c.nome, c.email) LIKE '%" . utf8_decode(implode('', explode(' - ', $aDados->oPesquisaTicket->cliente))) . "%' " : ''; 
			$aWherePesquisa[] = $aDados->oPesquisaTicket->data_inicial != '' ? "t.data >= '" . dateToSQL($aDados->oPesquisaTicket->data_inicial) . " 00:00:00'  " : ''; 
			$aWherePesquisa[] = $aDados->oPesquisaTicket->data_final != '' ? "t.data <= '" . dateToSQL($aDados->oPesquisaTicket->data_final) . " 23:59:59' " : '';			

			$aWherePesquisa[] = $aDados->oPesquisaTicket->data_finalizacao != '' ? "(t.ultima_resposta >= '" . dateToSQL($aDados->oPesquisaTicket->data_finalizacao) . " 00:00:00' AND t.ultima_resposta <= '" . dateToSQL($aDados->oPesquisaTicket->data_finalizacao) . " 23:59:59' AND status = 'Resolvido')" : '';

			$aWherePesquisa[] = $aDados->oPesquisaTicket->termo != '' ? "CONCAT(t.assunto, t.mensagem, t.codticket) LIKE ('%" . $aDados->oPesquisaTicket->termo ."%') " . " " : ''; 
			
			// Remove posições vazias pq eu não entendi nem o pq elas existem
			$aWherePesquisa = array_filter($aWherePesquisa, 'strlen');

			$sWherePesquisa = implode(' AND ', $aWherePesquisa);
		}

		if (count($aWherePesquisa) > 0) {
			$sWherePesquisa = " WHERE " . $sWherePesquisa;
		}

		$sFields = "c.nome as cliente_representante, c.empresa, t.*, ct.descricao as categoria, u.nome as atendente, DATEDIFF(CURDATE(), DATA) AS dias";
		$sTable = " tickets t ";

		$sWhere = " LEFT JOIN clientes c ON c.codcliente = t.codcliente";
		$sWhere .= " LEFT JOIN usuarios u ON u.codusuario = t.codusuario ";
		$sWhere .= " INNER JOIN categorias ct ON ct.codcategoria = t.codcategoria ";
		$sWhere .= $sWherePesquisa;
		$sWhere .= " ORDER BY data DESC ";

		$aTickets = $this->getData($sTable, $sWhere, $sFields, '', 'array');


		foreach ($aTickets as $key => $value) {	
			$aTickets[$key]['data'] = dateToHtml($aTickets[$key]['data'], false);
			$aTickets[$key]['hash_link'] = md5($aTickets[$key]['codticket']);
		}
		
		echo json_encode($aTickets);
	}

	public function getTicketsBacklog(){

		$sFields = "c.nome as cliente_representante, c.empresa, t.*, ct.descricao as categoria, u.nome as atendente, DATEDIFF(CURDATE(), DATA) AS dias, t.anexos";
		$sTable = " tickets t ";

		$sWhere = " LEFT JOIN clientes c ON c.codcliente = t.codcliente ";
		$sWhere .= " LEFT JOIN usuarios u ON u.codusuario = t.codusuario ";		
		$sWhere .= " INNER JOIN categorias ct ON ct.codcategoria = t.codcategoria ";		
		$sWhere .= " WHERE LEFT(data, 10) <> CURDATE() ";
		$sWhere .= " AND t.status <> 'Resolvido' ";
		$sWhere .= " AND t.status <> 'Expirado' ";
		$sWhere .= " AND ct.descricao <> 'Desenvolvimento' ";
		$sWhere .= " ORDER BY data ASC ";

		$aTickets = $this->getData($sTable, $sWhere, $sFields, '', 'array');


		foreach ($aTickets as $key => $value) {
			$aTickets[$key]['data'] = dateToHtml($aTickets[$key]['data'], false);
			$aTickets[$key]['ultima_resposta'] = dateToHtml($aTickets[$key]['ultima_resposta'], false);
			$aTickets[$key]['hash_link'] = md5($aTickets[$key]['codticket']);
		}
		
		echo json_encode($aTickets);
	}


	public function getTicketsToday(){

		$sFields = "c.nome as cliente_representante, c.empresa, t.*, ct.descricao as categoria, u.nome as atendente, SUBSTRING(DATA, 12, 5) AS hora, t.anexos";
		$sTable = " tickets t ";

		$sWhere = " LEFT JOIN clientes c ON c.codcliente = t.codcliente ";
		$sWhere .= " LEFT JOIN usuarios u ON u.codusuario = t.codusuario ";
		$sWhere .= " INNER JOIN categorias ct ON ct.codcategoria = t.codcategoria ";	
		$sWhere .= " AND t.status <> 'Resolvido' ";
		$sWhere .= " AND t.status <> 'Expirado' ";
		$sWhere .= " AND ct.descricao <> 'Desenvolvimento' ";	
		$sWhere .= " WHERE LEFT(data, 10) = CURDATE() ORDER BY data DESC";

		$aTickets = $this->getData($sTable, $sWhere, $sFields, '', 'array');

		foreach ($aTickets as $key => $value) {		
			$aTickets[$key]['ultima_resposta'] = dateToHtml($aTickets[$key]['ultima_resposta'], true, true);
			$aTickets[$key]['hash_link'] = md5($aTickets[$key]['codticket']);
		}
			

		echo json_encode($aTickets);
	}

	public function getTicketsMelhorias(){

		$sFields = "c.nome as cliente_representante, c.empresa, t.*, ct.descricao as categoria, u.nome as atendente, DATEDIFF(CURDATE(), DATA) AS dias, t.anexos";
		$sTable = " tickets t ";

		$sWhere = " LEFT JOIN clientes c ON c.codcliente = t.codcliente ";
		$sWhere .= " LEFT JOIN usuarios u ON u.codusuario = t.codusuario ";		
		$sWhere .= " INNER JOIN categorias ct ON ct.codcategoria = t.codcategoria ";				
		$sWhere .= " WHERE status <> 'Resolvido' ";
		$sWhere .= " AND ct.descricao = 'Desenvolvimento' ";
		$sWhere .= " ORDER BY data ASC ";

		$aTickets = $this->getData($sTable, $sWhere, $sFields, '', 'array');


		foreach ($aTickets as $key => $value) {
			$aTickets[$key]['data'] = dateToHtml($aTickets[$key]['data'], false);
			$aTickets[$key]['ultima_resposta'] = dateToHtml($aTickets[$key]['ultima_resposta'], false);
			$aTickets[$key]['hash_link'] = md5($aTickets[$key]['codticket']);
		}
		
		echo json_encode($aTickets);
	}

	public function getTicketsByCliente(){

		$aCliente = Session::get('user');
		$sqlAddCliente = $aCliente['tipo'] == 'cliente' ? " AND t.codcliente = '" . $aCliente['codcliente'] . "'" : '';


		$sFields = "c.nome as cliente_representante, c.empresa, t.*, ct.descricao as categoria, u.nome as atendente, SUBSTRING(DATA, 12, 5) AS hora";
		$sTable = " tickets t ";

		$sWhere = " LEFT JOIN clientes c ON c.codcliente = t.codcliente ";
		$sWhere .= " LEFT JOIN usuarios u ON u.codusuario = t.codusuario ";
		$sWhere .= " INNER JOIN categorias ct ON ct.codcategoria = t.codcategoria ";	
		// $sWhere .= " AND t.status <> 'Resolvido' ";
		$sWhere .= $sqlAddCliente;
		$sWhere .= " ORDER BY data DESC";

		$aTickets = $this->getData($sTable, $sWhere, $sFields, '', 'array');

		foreach ($aTickets as $key => $value) {		
			$aTickets[$key]['ultima_resposta'] = dateToHtml($aTickets[$key]['ultima_resposta'], true, true);
			$aTickets[$key]['hash_link'] = md5($aTickets[$key]['codticket']);
			$aTickets[$key]['data'] = dateToHtml($aTickets[$key]['data'], false);
		}
			

		echo json_encode($aTickets);
	}


	public function getTicket($codigo){

		$sWhere = " LEFT JOIN categorias c ON c.codcategoria = t.codcategoria 
					LEFT JOIN tickets_tipos tt ON t.codtipo = tt.codtipo
					WHERE MD5(t.codticket) = '" . $codigo . "'";

		$aTicket = $this->getData("tickets t", $sWhere, "t.*, c.descricao as categoria, tt.descricao as tipo", '');

		if (empty($aTicket['codticket'])) {
			echo json_encode(array('msg' => 'false'));
			return;
		}

		$aCliente = $this->getData('clientes', "WHERE codcliente = '" . $aTicket['codcliente'] . "'", '','');
		$aTicket['cliente'] = $aCliente;

		// Usu?io do ticket 
		$aUsuario = $this->getData('usuarios', "WHERE codusuario = '" . $aTicket['codusuario'] . "'", '','');
		$aTicket['usuario'] = $aUsuario;


		// Útimos tickets do cliente do ticket
		
		$cliente = new Cliente();
		$aTicket['cliente']['ultimos_tickets'] = $cliente->getUltimosTicketsCliente($aTicket['cliente']['empresa']);


		// Notas do ticket
		$aTicket['notas'] = self::getNotas($aTicket['codticket']);


		// Todos status existentes
		$aTicket['todos_status'] = $this->getData('tickets_status', "WHERE ativo = 'S'", '', 'array');
		

		// Todas categorias existentes
		$aTicket['todas_categorias'] = $this->getData('categorias', "WHERE ativo = 'S'", '', '', 'array');


		// Todos tipos
		$aTicket['todos_tipos'] = $this->getData('tickets_tipos', "WHERE ativo = 'S'", '', '', 'array');


		// Todas usu?ios existentes
		$aTicket['todos_usuarios'] = $this->getData('usuarios', "WHERE ativo = 'S'", '', '', 'array');

		// Todas logs do ticket
		$aTicket['logs'] = $this->getData('tickets_log', "WHERE codticket = '" . $aTicket['codticket'] . "' ORDER BY data ASC", 'status_ticket, tipo, tipo_usuario, nome_usuario, data', '', 'array');


		// Mensagens trocadas no ticket
		$aMensagens = $this->getData("tickets_mensagens tm

									LEFT JOIN clientes c 
									ON c.codcliente = tm.codcliente

									LEFT JOIN usuarios u
									ON u.codusuario = tm.codusuario", 

									"WHERE tm.codticket = '" . $aTicket['codticket'] . "' ORDER BY tm.data ASC",

									"tm.*, IF(u.nome <> '', u.nome, c.nome) AS responsavel",

									'', 

									'array');		


		// Dá um trato nos anexos
		$aTicket['anexos'] = organizaAnexos($aTicket['anexos']);


		foreach ($aMensagens as $key => $value) {
			$aMensagens[$key]['data'] = dateToHtml($aMensagens[$key]['data']);		
			$aMensagens[$key]['anexos'] = organizaAnexos($aMensagens[$key]['anexos']);
		}


		$aTicket['mensagens'] = $aMensagens;

		$aTicket['data'] = dateToHtml($aTicket['data']);


		echo json_encode($aTicket);
		
	}
    
	public function insertTicket($q, $aDados, $aFiles){

		$notificar_cliente = $aDados->oTicket->notificar_cliente;
		unset($aDados->oTicket->notificar_cliente);


		if (!$aDados->oTicket->cliente) {
			$aCliente =  Session::get('user');
			unset($aCliente['recursos']);
			unset($aCliente['menu']);
		} else {
			$auxCliente = explode(' - ', $aDados->oTicket->cliente);
			$aCliente = $this->getData('clientes', "WHERE email = '" . $auxCliente[2]. "'", '', '', '');
			unset($aCliente['recursos']);
			unset($aCliente['menu']);
		}

		$aDados->oTicket->codcliente = $aCliente['codcliente'];
		

		unset($aDados->oTicket->modelo);
		unset($aDados->oTicket->cliente);

		// Se for um usuario abrindo, joga o ticket pra ele. Se nao, busca auto-atribuicao no cadastro de usuarios e joga pra ele.
		$userSession = Session::get('user');
		if ($userSession['tipo'] == 'usuario') {			
			$aDados->oTicket->codusuario = $userSession['codusuario'];
		} else {
			$sAutoAtribuir = $this->getData('usuarios', "WHERE ativo = 'S' AND auto_atribuir = 'S'", 'codusuario', '', '');		
			$aDados->oTicket->codusuario = $sAutoAtribuir['codusuario'];
		}


		$aFiles = array_filter($aFiles, 'count');

		// Mandinga fortíssima. O redirect manda pra pasta TEMP e quando o usu?io clica em INSERIR TICKET esse foreach move do temp pra pasta do ano m? do ticket
		foreach ($aFiles as $key => $anexo) {

			$pasta = date('Ym');

			// Vê se a pasta existe e cria ela
			if (!is_dir(SERVER_PATH . "tickets_anexos/" . $pasta)) {
				@mkdir(SERVER_PATH . "tickets_anexos/" . $pasta);
			}

	        $arquivo = $anexo['file']['name'];
	        $arquivo = replaceAcentos($arquivo);
	        $arquivo = str_replace(' ', '', $arquivo);


			$tempPath = SERVER_PATH . "temp/" . basename($arquivo);
			$uploadPath = SERVER_PATH . "tickets_anexos/" . $pasta . "/" . date('Ymdhis') . basename($arquivo);	    		    

			if (count($aFiles) == $key) {
				$sConcatFiles .= $pasta . "/" . date('Ymdhis') . basename($arquivo);				
			} else {
				$sConcatFiles .= $pasta . "/" . date('Ymdhis') . basename($arquivo) . ",";
			}
			
	        if (copy($tempPath , $uploadPath)) {
	        	// Unlink remove os arquivos da pasta temp
	        	@unlink($tempPath);
	            echo "Arquivo válido e upload feito com sucesso.\n";
	        } else {	        	
	            echo "Upload FALHOU";
	        }
		}

		unset($_SESSION['anexos']);		
		$aDados->oTicket->anexos = $sConcatFiles;

		$sSet = buildSet($aDados);
		$sSet.= ", data = NOW()";


		$aDados->oTicket->codticket = $this->insertData($this->sTable, $sSet, '');

		// VERIFICA SE O CLIENTE POSSUI NOTAS NO CADASTRO E INSERE O TICKET JÁ COM UMA NOTA INICIAL
		$sNotaCliente = $this->getData('clientes', "WHERE empresa = '" . $aCliente['empresa'] . "' AND (nota IS NOT NULL OR nota <> '') LIMIT 1", 'nota', '', '');
		if ($sNotaCliente['nota'] !== '' && !is_null($sNotaCliente['nota'])) {
			$aDados->oTicket->nova_nota = "<b>Nota originada do cadastro do cliente:</b> <br />" . $sNotaCliente['nota'] . "";
			self::insertNota($q, $aDados);
		}
		
		$aDados->oTicket->cliente->empresa = $aCliente['empresa'];
		$aDados->oTicket->cliente->nome = $aCliente['nome'];
		$aDados->oTicket->cliente->email = $aCliente['email'];


		if ($notificar_cliente == 1) {			
			sendEmail($aDados->oTicket->codticket, $aCliente['nome'], $aCliente['email'], $aCliente['empresa'], $aDados->oTicket->mensagem, $anexo="", 'ticketRecebido');
		}			

		$Generic = new Generic();
		$Generic->insertLog(
				$aDados->oTicket->codticket, 
				"Novo", 
				"insert_ticket_novo",
				$userSession['tipo'],
				$userSession['nome'],
				$aDados);

		echo json_encode(array('codticket' => md5($aDados->oTicket->codticket)));
	}


	public function updateTicket($q, $aDados, $aFiles = ''){

		$codticket = $aDados->oTicket->codticket;
		
		// Pega são codusuario do objeto usuario dentro do objeto ticket e depois da UNSET no ticket->usuario
		$aDados->oTicket->codusuario = $aDados->oTicket->usuario->codusuario;
		$codtipo = $aDados->oTicket->codtipo;

		unset($aDados->oTicket->codticket);
		unset($aDados->oTicket->cliente);
		unset($aDados->oTicket->codcliente);
		unset($aDados->oTicket->usuario);
		unset($aDados->oTicket->todos_status);
		unset($aDados->oTicket->notas);
		unset($aDados->oTicket->mensagens);
		unset($aDados->oTicket->categoria);
		unset($aDados->oTicket->todas_categorias);
		unset($aDados->oTicket->todos_tipos);
		unset($aDados->oTicket->tipo);
		unset($aDados->oTicket->todos_usuarios);
		unset($aDados->oTicket->data);
		unset($aDados->oTicket->anexos);
		unset($aDados->oTicket->logs);
		unset($aDados->oTicket->codtipo);


        $sSet = buildSet($aDados);

        $sSet .= ", dtalteracao = NOW(), ultima_resposta = NOW()";

		$sWhere = "WHERE codticket = '" . $codticket . "' ";


		// Checa se o ticket já está como tipo 1 (Proxy Systems)
		$checkSeJaEhProxy = $this->getData("tickets", "WHERE codticket = '" . $codticket . "'", 'codtipo', '', '');
		// Transfere pra Proxy/Safety
		if ($codtipo == '1' && $checkSeJaEhProxy['codtipo'] != $codtipo) {
			if ($this->transfereTicket('transferir_para_proxy', $codticket, '1')) {
				echo json_encode(array('msg' => 'Ticket transferido para ' . $tipo));
			}
		} else {
			die($codtipo . "|" . $checkSeJaEhProxy['codtipo']);
		}

		$return = $this->updateData($this->sTable, $sWhere, $sSet, '');
	}
	

	public function updateTicketStatus($q, $aDados){

		$sSet = "SET status = '" . $aDados->oTicket->status . "' ";						
		$sWhere = "WHERE codticket = '" . $aDados->oTicket->codticket . "' ";
		
		$userSession = Session::get('user');
		$Generic = new Generic();
		$Generic->insertLog(
				$aDados->oTicket->codticket, 
				$aDados->oTicket->status, 
				"update_ticket_status",
				$userSession['tipo'],
				$userSession['nome'],
				$aDados);

		$return = $this->updateData($this->sTable, $sWhere, $sSet, '');

	}	

	public function updateTicketUltimaResposta($codticket, $respondente){

		$sSet = "SET ultima_resposta = NOW(), ultimo_respondente = '" . $respondente . "' ";
		$sWhere = "WHERE codticket = '" . $codticket . "' ";
		$return = $this->updateData($this->sTable, $sWhere, $sSet, '');

	}

	public function insertNovaResposta($q, $aDados, $aFiles){

		// Atualiza o status do ticket de acordo com o botão na tela
		$sReturn = self::updateTicketStatus('', $aDados);

		$sTable = "tickets_mensagens";

		unset($aDados->oTicket->modelo);
		unset($aDados->oTicket->cliente->ultimos_tickets);
		

		$aFiles = array_filter($aFiles, 'count');

		// Mandinga fortíssima. O redirect manda pra pasta TEMP e quando o usu?io clica em INSERIR TICKET esse foreach move do temp pra pasta do ano mês do ticket
		foreach ($aFiles as $key => $anexo) {

			$pasta = date('Ym');

			// Vê se a pasta existe e cria ela
			if (!is_dir(SERVER_PATH . "tickets_anexos/" . $pasta)) {
				@mkdir(SERVER_PATH . "tickets_anexos/" . $pasta);
			}

	        $arquivo = $anexo['file']['name'];
	        $arquivo = replaceAcentos($arquivo);
	        $arquivo = str_replace(' ', '', $arquivo);


			$tempPath = SERVER_PATH . "temp/" . basename($arquivo);
			$uploadPath = SERVER_PATH . "tickets_anexos/" . $pasta . "/" . date('Ymdhis') . basename($arquivo);
			
			$sConcatFiles .= $pasta . "/" . date('Ymdhis') . basename($arquivo) . ",";

	        if (copy($tempPath , $uploadPath)) {
	        	// Unlink remove os arquivos da pasta temp
	        	@unlink($tempPath);
	            echo "Arquivo váido e upload feito com sucesso.\n";
	        } else {	        	
	            echo "Upload FALHOU";
	        }

		}

		unset($_SESSION['anexos']);		
		$aDados->oTicket->anexos = $sConcatFiles;


		// Verifica se a resposta vem de usuário ou de cliente.
		$userSession = Session::get('user');
		if ($userSession['tipo'] == 'cliente') {
			$sql_add_respondente = ", codcliente = '" . utf8_decode($userSession['codcliente']) . "'";			
		} else {
			$sql_add_respondente = ", codusuario = '" . utf8_decode($userSession['codusuario']) . "'";
		}

		// Atualize data da última resposta e quem foi o respondente (usuario ou cliente)
		$sReturn = self::updateTicketUltimaResposta($aDados->oTicket->codticket, $userSession['tipo']);

		$aDados->oTicket->nova_resposta .= ($userSession['tipo'] == 'usuario') ? $userSession['assinatura'] : '';

		$sSet = "SET codticket = '" . $aDados->oTicket->codticket . "' 				
				, mensagem = '" . str_replace("'", "", $aDados->oTicket->nova_resposta) . "' 
				, responsavel = '" . str_replace("'", "", $aDados->oTicket->cliente->nome) . "'
				, anexos = '" . $sConcatFiles . "'
				, data = NOW() "
				. $sql_add_respondente;


		$this->insertData($sTable, $sSet, '');


		sendEmail($aDados->oTicket->codticket, $aDados->oTicket->cliente->nome, $aDados->oTicket->cliente->email, $aDados->oTicket->cliente->empresa, $aDados->oTicket->nova_resposta, $anexo="", 'ticketRespondido');

		unset($aDados->oTicket->todos_usuarios);
		unset($aDados->oTicket->todas_categorias);
		unset($aDados->oTicket->todos_status);

		$Generic = new Generic();
		$Generic->insertLog(
				$aDados->oTicket->codticket, 
				$aDados->oTicket->status, 
				"insert_ticket_resposta",
				$userSession['tipo'],
				$userSession['nome'],
				$aDados);

	}

	public function getTicketTotaisDia(){

		$sTable = "tickets t INNER JOIN categorias c ON c.codcategoria = t.codcategoria";
		$sWhere = "WHERE LEFT(t.data, 10) = CURDATE() AND c.descricao <> 'Desenvolvimento' ";
		

		$aTotais = array();
		$aTotais['total_dia'] = $this->getData($sTable, $sWhere, 'count(*) as total_dia', '', '');
		$aTotais['total_resolvidos'] = $this->getData($sTable, $sWhere . " AND t.status = 'Resolvido'", 'count(*) as total_resolvidos', '', '');
			

		$total_mes = $this->getData($sTable, "WHERE t.data BETWEEN CONCAT(SUBSTRING(CURDATE(),1, 8), '01', ' 00:00:00') AND CONCAT(LAST_DAY(CURDATE()), ' 23:59:59') AND c.descricao <> 'Desenvolvimento'", 'count(*) as total_mes', '', '');		
		$total_mes_resolvido = $this->getData($sTable, "WHERE t.data BETWEEN CONCAT(SUBSTRING(CURDATE(),1, 8), '01', ' 00:00:00') AND CONCAT(LAST_DAY(CURDATE()), ' 23:59:59') AND t.status = 'Resolvido' AND c.descricao <> 'Desenvolvimento'", 'count(*) as total_mes_resolvido', '', '');
			
		$aTotais['eficiencia_mes'] = ceil($total_mes_resolvido['total_mes_resolvido'] / $total_mes['total_mes'] * 100);
		

		$aTotais['abertos_mes'] = $this->getData($sTable, "WHERE t.data BETWEEN CONCAT(SUBSTRING(CURDATE(),1, 8), '01', ' 00:00:00') AND CONCAT(LAST_DAY(CURDATE()), ' 23:59:59') AND c.descricao <> 'Desenvolvimento'", 'count(*) as abertos_mes', '', '');
		$aTotais['resolvidos_mes'] = $total_mes_resolvido['total_mes_resolvido'];

		echo json_encode($aTotais);
	}


	public function getTicketTotaisBacklog(){

		$sTable = "tickets t INNER JOIN categorias c ON c.codcategoria = t.codcategoria";
		$sWhere = "WHERE t.data 
						BETWEEN CONCAT(SUBSTRING(CURDATE(), 1, 8), '01', ' 00:00:00')
						AND CONCAT(LAST_DAY(CURDATE()), ' 23:59:59')
						AND SUBSTR(t.ultima_resposta, 1, 10) > SUBSTR(t.data, 1, 10)
						AND c.descricao <> 'Desenvolvimento' ";			


		$aTotais = array();
		$aTotais['total_backlog_mes'] = $this->getData($sTable, $sWhere, 'count(*) as total_mes', '', '');
		$aTotais['total_resolvidos'] = $this->getData($sTable, $sWhere . " AND t.status = 'Resolvido' AND SUBSTR(t.ultima_resposta, 1, 10) > SUBSTR(t.data, 1, 10) AND c.descricao <> 'Desenvolvimento'", 'count(*) as total_resolvidos', '', '');

		$total_mes = $this->getData($sTable,           "WHERE t.data BETWEEN CONCAT(SUBSTRING(CURDATE(), 1, 8), '01', ' 00:00:00') AND CONCAT(LAST_DAY(CURDATE()), ' 23:59:59') AND SUBSTR(t.ultima_resposta, 1, 10) > SUBSTR(t.data, 1, 10) AND c.descricao <> 'Desenvolvimento'", 'count(*) as total_mes', '', '');		
		$total_mes_resolvido = $this->getData($sTable, "WHERE t.data BETWEEN CONCAT(SUBSTRING(CURDATE(), 1, 8), '01', ' 00:00:00') AND CONCAT(LAST_DAY(CURDATE()), ' 23:59:59') AND SUBSTR(t.ultima_resposta, 1, 10) > SUBSTR(t.data, 1, 10) AND c.descricao <> 'Desenvolvimento' AND t.status = 'Resolvido'", 'count(*) as total_mes_resolvido', '', '');
			
		$aTotais['eficiencia_mes'] = ceil($total_mes_resolvido['total_mes_resolvido'] / $total_mes['total_mes'] * 100);

		echo json_encode($aTotais);
	}

	// NOTAS
	public function insertNota($q, $aDados){		

		$sTable = "tickets_notas";

		$sSet = "SET codusuario = '" . $aDados->oTicket->codusuario . "', codticket = '" . $aDados->oTicket->codticket . "', mensagem = '" . utf8_decode(str_replace("'", "", $aDados->oTicket->nova_nota)) . "'";

		$sSet.= ", data = NOW()";		
		
		$this->insertData($sTable, $sSet, '');

	}

	public function updateNota($q, $aDados){		

		$sTable = "tickets_notas";

		$sSet = "SET lido = IF(lido = 'S', 'N', 'S') ";

		$this->insertData($sTable, $sSet, 'morre');

	}

	public function getNotas($codigo){

		$sFields = "tn.*, u.nome as usuario";
		$sTable = " tickets_notas tn ";
		
		$sWhere .= " LEFT JOIN usuarios u ON u.codusuario = tn.codusuario ";				
		$sWhere .= " WHERE tn.codticket = '" . $codigo . "' ";
		$sWhere .= " ORDER BY data ASC ";

		$aNotas = $this->getData($sTable, $sWhere, $sFields, '', 'array');


		foreach ($aNotas as $key => $value) {
			$aNotas[$key]['data'] = dateToHtml($aNotas[$key]['data']);
		}
		
		return $aNotas;
	}	


	public function getNotificacoes($codigo){

		$sFields = "u.nome, tn.*, TIMEDIFF(NOW(), tn.data) AS tempo, md5(t.codticket) as hash_link";
		$sTable = " tickets_notas tn ";
		
		$sWhere .= " INNER JOIN tickets t ON t.codticket = tn.codticket ";				
		$sWhere .= " INNER JOIN usuarios u ON u.codusuario = tn.codusuario ";				
		$sWhere .= " WHERE t.codusuario = '" . $codigo . "' AND tn.codusuario <> '" . $codigo . "' ";
		$sWhere .= " AND t.status <> 'Resolvido' ";
		$sWhere .= " ORDER BY tn.data DESC ";

		$aNotas = $this->getData($sTable, $sWhere, $sFields, '', 'array');

		foreach ($aNotas as $key => $value) {

			$auxTempo = explode(':', $aNotas[$key]['tempo']);
			
			if ($auxTempo[0] != '00') {			
				$sTempo .= $auxTempo[0] . " horas atrás";
			} else {
				$sTempo .= $auxTempo[1] . " minutos atrás";				
			}

			$aNotas[$key]['tempo'] = $sTempo;

			$sTempo = '';
		}

		
		return $aNotas;
	}



	public function transfereTicket($tipo_transferencia, $codticket, $codtipo){
		
		switch ($tipo_transferencia) {
			case 'transferir_para_proxy':									

					$aTicketSafety = $this->getData("tickets", "WHERE codticket = '$codticket'", "", '', '');
					$aUsuarioSafety = $this->getData("usuarios", "WHERE codusuario = '" . $aTicketSafety['codusuario'] . "'", "email", '', '');
					$aTicketRespostasSafety = $this->getData("tickets_mensagens", "WHERE codticket = '$codticket'", '', '', '');


					$Dao = new Dao('db_suporteproxy');
									
					// Usuario da SAFETY sera o CLIENTE no proxy
					$aClienteProxy = $Dao->getData("clientes", "WHERE email = '" . $aUsuarioSafety['email'] . "'", 'codcliente', '', '');				

					// Busca usuario auto atribuição na proxy
					$sAutoAtribuir = $Dao->getData('usuarios', "WHERE ativo = 'S' AND auto_atribuir = 'S'", 'codusuario', '', '');		

					$sSet = "SET
								codusuario = '" . $sAutoAtribuir['codusuario'] . "',
								codcliente = '" . $aClienteProxy['codcliente'] . "',
								codcategoria = '" .  $aTicketSafety['codcategoria'] . "',
								assunto = '" . utf8_decode($aTicketSafety['assunto']) . "',
								mensagem = '" . utf8_decode($aTicketSafety['mensagem']) . "',
								status = '" . utf8_decode($aTicketSafety['status']) . "',
								anexos = '" . $aTicketSafety['anexos'] . "',
								data = '" . $aTicketSafety['data'] . "',
								ultima_resposta = '" . $aTicketSafety['ultima_resposta'] . "',
								codtipo = '$codtipo',								
								ultimo_respondente = '" . utf8_decode($aTicketSafety['ultimo_respondente']) . "'";
										
					$codticket_novo = $Dao->insertData("tickets", $sSet, '');

					// INSERIR RESPOSTAS
					if (count($aTicketRespostasSafety) >= 1) {						
						foreach ($aTicketRespostasSafety as $key => $mensagem) {
							$sSet = "SET
								codticket = '" . $codticket_novo . "',
								codcliente = '" . $aClienteProxy['codcliente'] . "',
								mensagem = '" . utf8_decode($mensagem['mensagem']) . "',
								data = '" . $mensagem['data'] . "',
								responsavel = '" . utf8_decode($mensagem['responsavel']) . "',
								anexos = '" . $mensagem['anexos'] . "'";

							$Dao->insertData("tickets_mensagens", $sSet);
						}
					}
			
					return true;
				break;
			default:
				echo "Sem tipo definido";
				break;
		}
		
	}
    
}

?>