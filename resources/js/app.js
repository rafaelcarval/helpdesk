﻿// Lucas - Suporte Help Desk 21/08/2017


// var SERVER_PATH = 'http://192.168.10.20/safetydocs_suporte/server/';
// var SERVER_PATH = 'http://localhost:8080/safetydocs_suporte/server/';
var SERVER_PATH = 'server/';


var app = angular.module('safetydocs_suporte', ['ui.router','ngMask','ngSanitize','720kb.datepicker','thatisuday.dropzone','ngAnimate'])
    



.config(function($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise("app/TicketsCliente");
    $stateProvider        
        .state('navbar_completa', {
          url: "/app",
          templateUrl: "partials/navbar.html",
          controller: "userCtrl"
        })
        .state('login', {
          url: "/Login",
          templateUrl: "partials/login.html",
          controller: "userCtrl"
        })         
        .state('cliente_cadastro', {
          url: "/CadastroCliente",
          templateUrl: "partials/cliente_cadastro.html",
          controller: "userCtrl"
        })
        .state('navbar_completa.config', {
          url: "/Configuracoes",
          templateUrl: "pages/config.html",
          controller: "configCtrl"
        })           


        .state('navbar_completa.tickets', {
          url: "/Tickets",
          templateUrl: "pages/ticket/tickets.html",
          controller: "ticketCtrl"
        })     
        .state('navbar_completa.tickets_clientes', {
          url: "/TicketsCliente",
          templateUrl: "pages/ticket/tickets_cliente.html",
          controller: "ticketCtrl"
        })   


        .state('navbar_completa.melhorias', {
          url: "/Melhorias",
          templateUrl: "pages/ticket/tickets_melhorias.html",
          controller: "ticketCtrl"
        }) 
        .state('navbar_completa.backlog', {
          url: "/Backlog",
          templateUrl: "pages/ticket/backlog.html",
          controller: "ticketCtrl"
        }) 
        .state('navbar_completa.ticket_pesquisa', {
          url: "/TicketPesquisa",
          templateUrl: "pages/ticket/ticket_pesquisa.html",
          controller: "ticketCtrl"
        })



        .state('navbar_completa.clientes', {
          url: "/Clientes",
          templateUrl: "pages/cliente/clientes.html",
          controller: "ticketCtrl"
        })         
        .state('navbar_completa.perfil_cliente', {
          url: "/ClientePerfil/:id",
          templateUrl: "pages/cliente/perfil_cliente.html",
          controller: "userCtrl"
        }) 

        .state('navbar_completa.cliente', {
          url: "/Cliente/:id",
          templateUrl: "pages/cliente/perfil_cliente.html",
          controller: "ticketCtrl"
        }) 
        
        .state('navbar_completa.novo_ticket_cliente', {
          url: "/NovoTicket",
          templateUrl: "pages/ticket/novo_ticket_cliente.html",
          controller: "ticketCtrl"
        }) 
        .state('navbar_completa.novo_ticket_admin', {
          url: "/NovoTicketAdmin",
          templateUrl: "pages/ticket/novo_ticket_admin.html",
          controller: "ticketCtrl"
        }) 



        .state('navbar_completa.modelos_resposta', {
          url: "/Modelos",
          templateUrl: "pages/ticket/modelos_resposta.html",
          controller: "ticketCtrl"
        })     
        .state('navbar_completa.categorias', {
          url: "/Categorias",
          templateUrl: "pages/ticket/categorias.html",
          controller: "ticketCtrl"
        })    

        .state('navbar_completa.ver_ticket_admin', {
          url: "/VerTicketAdmin/:id",
          templateUrl: "pages/ticket/ver_ticket_admin.html",
          controller: "ticketCtrl"
        })         
        .state('navbar_completa.ver_ticket_cliente', {
          url: "/VerTicketCliente/:id",
          templateUrl: "pages/ticket/ver_ticket_cliente.html",
          controller: "ticketCtrl"
        })


        .state('navbar_completa.evolucao', {
          url: "/Evolucao",
          templateUrl: "pages/graficos/graficos.html",
          controller: "graficosCtrl"
        })


        .state('navbar_completa.perfil', {
            url: "/Perfil/:id",
            templateUrl: "pages/usuario/perfil.html",
            controller: "userCtrl",        
        }) 
        .state('navbar_completa.usuarios', {
          url: "/Usuarios",
          templateUrl: "pages/usuario/usuarios.html",
          controller: "userCtrl"
        })            
})

app.filter('nl2br', function($sce){
    return function(msg,is_xhtml) { 
        var is_xhtml = is_xhtml || true;
        var breakTag = (is_xhtml) ? '<br />' : '<br>';
        var msg = (msg + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1'+ breakTag +'$2');
        return $sce.trustAsHtml(msg);
    }
});

app.config(function(dropzoneOpsProvider){
    dropzoneOpsProvider.setOptions({
        url : 'server/redirect.php',
        acceptedFiles : 'image/*, .csv, .docx, .pdf, .ppt, .zip, .rar, .xml',
        addRemoveLinks : false,
        dictDefaultMessage : 'Clique aqui para anexar arquivos',
        dictRemoveFile : 'Remover',
        dictResponseError : 'Não foi possível fazer upload desse anexo',
        filename : 'anexo'          
    });
});


app.service("toast", function () {

    this.showToast = function(message, color, time = 1500){

        toastr.options = {
          "newestOnTop": true,
          "positionClass": "toast-top-right",
          "timeOut": time,
          "tapToDismiss": true
        }

        switch(color){
            case 'success':
                toastr.success(message);
                break;
            case 'warning':
                toastr.warning(message);        
                break;
            case 'error':
                toastr.error(message);
                break;
            default:
                toastr.info(message);
        }

    }

});


app.service("user", function ($rootScope, route, $http, toast, $location) {

    // Bruxaria para chamar função do service dentro do próprio service
    var _this = this;

    this.verificaUser = function(){

        var p = 'verificaUser';
        var classe = 'usuario';
        
        $http.get(SERVER_PATH + "redirect.php?p=" + p + "&classe=" + classe + "&location=" + $location.path()).success(function(result){              
              if (result.msg != 'false') {
                $rootScope.user = result;
                $rootScope.notificacoes = result.notificacoes;              
              } else {
                route.goRota("login");
              }
          });
    }       

    this.service_getUsuarios = function(pesquisa){

        var p = 'getUsuarios';
        var classe = 'usuario';

        $rootScope.loading = true;

        $http.get(SERVER_PATH + "redirect.php?p=" + p + "&classe=" + classe + "&pesquisa=" + pesquisa).success(function(result){
            $rootScope.usuarios = result;
            $rootScope.loading = false;
        });

    }

    this.service_getTodosRecursos = function(){

        var p = 'getTodosRecursos';
        var classe = 'generic';

        $rootScope.loading = true;

        $http.get(SERVER_PATH + "redirect.php?p=" + p + "&classe=" + classe + "&location=" + $location.path()).success(function(result){  
            $rootScope.recursos = result;
            $rootScope.loading = false;       
        });

    }

    this.service_insertUsuario = function(oUsuario){

        var p = 'insertUsuario';
        var classe = 'usuario';

        $http.post(SERVER_PATH + "redirect.php?p=" + p + "&classe=" + classe, {
            oUsuario: oUsuario
        }).success(function(result){
            toast.showToast("Usuário inserido.",'success');           
        });

    }

    this.service_insertCliente = function(oCliente){

        var p = 'insertCliente';
        var classe = 'cliente';

        $http.post(SERVER_PATH + "redirect.php?p=" + p + "&classe=" + classe, {
            oCliente: oCliente
        }).success(function(result){
            if (result.msg) {
                toast.showToast(result.msg, 'warning');
            } else {
                // O cadastro pode ser aberto pela tela de novo ticket ou pela tela de login, esse if só verifica em que tela o cara está e da a tratativa.
                if ($location.path() == "/app/NovoTicketAdmin") {                    
                    _this.service_getClientes();
                    // Bruxaria absurda pra fechar o modal. o .hide não funcionava direito.
                    $("#novo_cliente_modal .close").click();
                } else {
                    route.goRota('login');
                }                
            }
        });

    }

    this.service_getClientes = function(pesquisa){

        var p = 'getClientes';
        var classe = 'cliente';

        $rootScope.loading = true;

        $http.get(SERVER_PATH + "redirect.php?p=" + p + "&classe=" + classe + "&pesquisa=" + pesquisa).success(function(result){
            $rootScope.clientes = result;
            $rootScope.loading = false;
        });

    }

    this.service_updateUsuario = function(oUsuario){

        var p = 'updateUsuario';
        var classe = 'usuario';

        $http.put(SERVER_PATH + "redirect.php?p=" + p + "&classe=" + classe, {
            oUsuario: oUsuario
        }).success(function(result){
            toast.showToast("Usuário salvo.",'success');   
        });

    }

    this.service_destroySession = function(){
        var p = 'destroy';
        var classe = 'session';

        $http.get(SERVER_PATH + "redirect.php?p=" + p + "&classe=" + classe);
    }   

    // Clientes
    this.service_updateCliente = function(oCliente){

        var p = 'updateCliente';
        var classe = 'cliente';

        $http.put(SERVER_PATH + "redirect.php?p=" + p + "&classe=" + classe, {
            oCliente: oCliente
        }).success(function(result){
            if (result.msg) {                
                toast.showToast(result.msg, 'warning');
                return;
            } else {
                toast.showToast("Cliente salvo.",'success');           
                route.goRota('navbar_completa.clientes');
            }
        });

    }

});

app.service("date", function () {

    this.today = new Date();
    this.dd = this.today.getDate();
    this.mm = this.today.getMonth()+1;
    this.yyyy = this.today.getFullYear();

    this.dd = this.dd < 10 ? '0' + this.dd : this.dd;
    this.mm = this.mm < 10 ? '0' + this.mm : this.mm;

    this.getToday = function(){
        return this.dd + '/' + this.mm + '/' + this.yyyy;
    };

    this.getComecoMes = function(){    
        return '01' + '/' + this.mm + '/' + this.yyyy;
    };
});

app.service("route", function ($rootScope, $location, $state, $stateParams) {

    $rootScope.loading = true;

    this.reloadPage = function(){
        $state.reload();        
        $rootScope.loading = false;
    };
    
    this.goRota = function(rota, id = 0){
        if (rota) {
            $state.go(rota, {id: id});
        }
        $rootScope.loading = false;
    };   

    this.back = function(){        
        window.history.back();
        $rootScope.loading = false;
    };

});    

app.factory('graficosProvider', function ($http, $location) {
    return {
        getGraficoCategorias: function(){
            return $http.get(SERVER_PATH + "redirect.php?p=getGraficoCategorias&classe=grafico&location=" + $location.path());        
        },
        getGraficoAbertosResolvidosLine: function(){
            return $http.get(SERVER_PATH + "redirect.php?p=getGraficoAbertosResolvidosLine&classe=grafico&location=" + $location.path());        
        }
    }
});

app.factory('empresaProvider', function ($http, $location) {
    return {
        getConfiguracoes: function(){
            return $http.get(SERVER_PATH + "redirect.php?p=getConfiguracoes&classe=generic&location=" + $location.path());        
        },     
        updateConfiguracoes: function(oConfiguracao){
            return $http.put(SERVER_PATH + "redirect.php?p=updateConfiguracoes&classe=generic", {
                oConfiguracao: oConfiguracao
            }); 

        }
    }
});

app.factory('anexosFactory', function ($http) {
    return {
        getAnexo: function(oAnexo){
            document.location.href = SERVER_PATH + "download.php?f=" + oAnexo.nome_original + "&p=" + oAnexo.pasta;
            // return $http.put(SERVER_PATH + "redirect.php?p=getAnexo&classe=anexo", {
            //     oAnexo: oAnexo
            // }); 
        }
    }
});

app.run(function(empresaProvider, user, $rootScope, route, $interval, $location) {
    
    $rootScope.loading = true;

    empresaProvider.getConfiguracoes().then(function(data){
        $rootScope.empresa = data.data;
        $rootScope.loading = false;
    });

    user.verificaUser();

    var counter = 0;
    var increaseCounter = function () {
        counter = counter + 1;        
        if (counter == 60) {
            // Se estiver nessas rotas de login, nao chama o verifica user
            if ($location.path() !== '/CadastroCliente' && $location.path() !== '/Login') {
                user.verificaUser();            
            }
            
            // A cada x segundos, quando passar pro aqui, a func vê se o usuário está em uma dessas páginas e, caso sim, da reload
            var locations_reload = ['/app/Tickets', '/app/TicketsCliente', '/app/Backlog', '/app/Melhorias'];
            
            if (locations_reload.indexOf($location.path()) !== -1) {
                route.reloadPage();                
            }

            counter = 0;
        }

    }

    $interval(increaseCounter, 1000);

});


app.controller("ticketCtrl", ['$scope', '$http', '$rootScope','route','toast','date','$location','anexosFactory', 'user',  function ($s, $http, $rs, route, toast, date, $location, anexosFactory, user) {

    $('[data-toggle="tooltip"]').tooltip();

    $s.hoje = date.getToday();
    $s.comeco_mes = date.getComecoMes();

    $s.dzOptions = {
        paramName : 'file',
        maxFilesize : '10',
            init: function() {
                this.on("success", function(responseText) {
                    console.log(responseText);                       
                });
            }
    };
    
    $s.dzCallbacks = {
           'addedfile': function(file) {
                $s.file = file;

                var aux = file.name.substring(0, 10);                     

                $s.$apply(function() {
                    $s.fileAdded = true;
                });

                var preview = document.getElementsByClassName('dz-preview');
                preview = preview[preview.length - 1];

                var imageName = document.createElement('span');
                imageName.innerHTML = aux;

                preview.insertBefore(imageName, preview.firstChild);

                var aux = '';

            },
            'success': function (file, response) {                
                $s.response_upload = response;         
            }
    };

    $s.getAnexo = function(anexo){
        anexosFactory.getAnexo(anexo);
    }

    $s.removerAnexos = function(){
        $s.dzMethods.removeAllFiles();
        $http.get(SERVER_PATH + "redirect.php?p=removerAnexos&classe=generic");
    }


    $s.selecionarCliente = function(cliente){
        $s.ticket = [];
        $s.ticket.cliente = cliente;
    } 

    $s.getTicket = function(){    

        var p = 'getTicket';
        var classe = 'ticket';      

        $rs.loading = true;
        
        $http.get(SERVER_PATH + "redirect.php?p=" + p + "&classe=" + classe + "&location=" + $location.path()).success(function(result){            
            $s.ticket = result; 
            $rs.loading = false;
        });

    }
    
    // TODOS OS TICKETS - PESQUISA
    $s.getTickets = function(oPesquisaTicket){       

        $s.p = 'getTickets';
        var classe = 'ticket';

        $rs.loading = true;

        $http.post(SERVER_PATH + "redirect.php?p=" + $s.p + "&classe=" + classe , {
            oPesquisaTicket: oPesquisaTicket
        }).success(function(result){            
            $s.tickets_pesquisa = result;
            $rs.loading = false;          
        });
    }

    // TICKETS BACKLOG (Abertos há mais de um 1 dia, mas ainda não resolvidos)
    $s.getTicketsBacklog = function(pesquisa_ticket){       
        $rs.p = 'getTicketsBacklog';
        var classe = 'ticket';

        $rs.loading = true;

        $http.get(SERVER_PATH + "redirect.php?p=" + $s.p + "&classe=" + classe).success(function(result){            
            $s.tickets_backlog = result;
            $s.getTicketTotaisBacklog();
            $rs.loading = false;          
        });
    }

    // TICKETS MELHORIAS ROADMAP
    $s.getTicketsMelhorias = function(pesquisa_ticket){
        $rs.p = 'getTicketsMelhorias';
        var classe = 'ticket';

        $rs.loading = true;

        $http.get(SERVER_PATH + "redirect.php?p=" + $s.p + "&classe=" + classe).success(function(result){            
            $s.tickets_melhorias = result;            
            $rs.loading = false;          
        });

    }   


    // TICKETS VISÃO DIA
    $s.getTicketsByCliente = function(pesquisa_ticket){
        $rs.p = 'getTicketsByCliente';
        var classe = 'ticket';

        $rs.loading = true;

        $http.get(SERVER_PATH + "redirect.php?p=" + $s.p + "&classe=" + classe).success(function(result){            
            $s.tickets_cliente = result;            
            $rs.loading = false;          
        });

    }   

    // TICKETS VISÃO DIA
    $s.getTicketsToday = function(pesquisa_ticket){
        $rs.p = 'getTicketsToday';
        var classe = 'ticket';

        $rs.loading = true;

        $http.get(SERVER_PATH + "redirect.php?p=" + $s.p + "&classe=" + classe).success(function(result){            
            $s.tickets_today = result;
            $s.getTicketTotaisDia();
            $rs.loading = false;          
        });

    }   


    // TOTAIS TOPO DA TELA
    $s.getTicketTotaisDia = function(){
        $rs.p = 'getTicketTotaisDia';
        var classe = 'ticket';

        $http.get(SERVER_PATH + "redirect.php?p=" + $s.p + "&classe=" + classe).success(function(result){            
            $s.tickets_totais_dia = result;            
        });    
    }      

    $s.getTicketTotaisBacklog = function(){
        $rs.p = 'getTicketTotaisBacklog';
        var classe = 'ticket';

        $http.get(SERVER_PATH + "redirect.php?p=" + $s.p + "&classe=" + classe).success(function(result){            
            $s.tickets_totais_backlog = result;            
        });    
    }  


    $s.getClientes = function(pesquisa = ''){        
        if (pesquisa == 'todos' || (pesquisa.length % 3) == 0) {
            user.service_getClientes(pesquisa);
        }
    }

    $s.getCliente = function(email){

        oCliente = [];
        oCliente.email = email;
    
        $s.p = 'getCliente';
        var classe = 'cliente';
      
        $http.post(SERVER_PATH + "redirect.php?p=" + $s.p + "&classe=" + classe, {
            oCliente: oCliente
        }).success(function(result){        
            $s.cliente = result;            
        });
    }    

    $s.getCategorias = function(){        
        $s.p = 'getCategorias';
        var classe = 'generic';

        $rs.loading = true;
        
        $http.get(SERVER_PATH + "redirect.php?p=" + $s.p + "&classe=" + classe).success(function(result){
            $s.categorias = result;   
            $rs.loading = false;
        });
    };


    $s.getModelos = function(){
        $s.p = 'getModelos';
        var classe = 'generic';

        $rs.loading = true;
        $http.get(SERVER_PATH + "redirect.php?p=" + $s.p + "&classe=" + classe).success(function(result){
            $s.modelos = result;    
            $rs.loading = false;
        });
    }

    $s.insertTicket = function (oTicket){

        $s.p = 'insertTicket';
        var classe = 'ticket';
        
        $rs.loading = true;

        $http.post(SERVER_PATH + "redirect.php?p=" + $s.p + "&classe=" + classe, {
            oTicket: oTicket
        }).success(function(result){
            toast.showToast("Ticket enviado!", "success");            

            if ($rs.user.tipo == 'usuario') {
                route.goRota('navbar_completa.ver_ticket_admin', result.codticket);
            } else {
                route.goRota('navbar_completa.ver_ticket_cliente', result.codticket);                
            }

            $s.blockEnviar = false;            
        });

    }     

    $s.insertNovaResposta = function (oTicket, status){

        if (!oTicket.nova_resposta || oTicket.nova_resposta == '' || typeof(oTicket.nova_resposta) == 'undefined') {
            toast.showToast("Digite uma resposta antes de enviar", "warning");
            return;
        }

        
        oTicket.responsavel = $rs.user.nome;
        oTicket.status = status;

        
        $s.blockEnviaResposta = true;

        $s.p = 'insertNovaResposta';
        var classe = 'ticket';
      
        $http.post(SERVER_PATH + "redirect.php?p=" + $s.p + "&classe=" + classe, {
            oTicket: oTicket
        }).success(function(result){    
            toast.showToast("Resposta enviada!", "success");                    
            $s.blockEnviaResposta = false;
            // route.goRota('navbar_completa.edit_ticket_admin');
            route.reloadPage();
        });

    }


    // Envia todo o ticket e trata do lado da classe
    $s.updateTicket = function (oTicket){

        $s.p = 'updateTicket';
        var classe = 'ticket';
            
        $rs.loading = true;

        $http.post(SERVER_PATH + "redirect.php?p=" + $s.p + "&classe=" + classe, {
            oTicket: oTicket
        }).success(function(result){    
            toast.showToast("Ticket atualizado!", "success");
            route.reloadPage();  
            $rs.loading = false;
        });
    }  

    $s.updateTicketStatus = function (codticket, status){

        $s.p = 'updateTicketStatus';
        var classe = 'ticket';

        var oTicket = {};
        oTicket.codticket = codticket;
        oTicket.status = status;
        
        $s.blockEnviaResposta = true;
        $rs.loading = true;

        $http.post(SERVER_PATH + "redirect.php?p=" + $s.p + "&classe=" + classe, {
            oTicket: oTicket
        }).success(function(result){    
            toast.showToast("Status atualizado!", "success");
            route.reloadPage();  
            $rs.loading = false;
            $s.blockEnviaResposta = false;
        });
    }   

    // Modelos
    $s.insertModelo = function (oModelo){

        $s.p = 'insertModelo';
        var classe = 'generic';
      
        $http.post(SERVER_PATH + "redirect.php?p=" + $s.p + "&classe=" + classe, {
            oModelo: oModelo
        }).success(function(result){    
            toast.showToast("Modelo inserido!", "success");
            $s.modelo = [];
            $s.getModelos();
        });
    }    

    $s.updateModelo = function (oModelo){

        $s.p = 'updateModelo';
        var classe = 'generic';
      
        $http.post(SERVER_PATH + "redirect.php?p=" + $s.p + "&classe=" + classe, {
            oModelo: oModelo
        }).success(function(result){    
            toast.showToast("Modelo atualizado!", "success");
            $s.getModelos();
        });
    }      

    $s.deleteModelo = function (oModelo){

        $s.p = 'deleteModelo';
        var classe = 'generic';
      
        $http.post(SERVER_PATH + "redirect.php?p=" + $s.p + "&classe=" + classe, {
            oModelo: oModelo
        }).success(function(result){    
            toast.showToast("Modelo deletado!", "success");
            $s.getModelos();
        });
    }  

    // Categorias
    $s.insertCategoria = function (oCategoria){

        $s.p = 'insertCategoria';
        var classe = 'generic';
      
        $http.post(SERVER_PATH + "redirect.php?p=" + $s.p + "&classe=" + classe, {
            oCategoria: oCategoria
        }).success(function(result){    
            toast.showToast("Categoria inserida!", "success");            
            $s.getCategorias();
        });
    }    

    $s.updateCategoria = function (oCategoria){

        $s.p = 'updateCategoria';
        var classe = 'generic';
      
        $http.post(SERVER_PATH + "redirect.php?p=" + $s.p + "&classe=" + classe, {
            oCategoria: oCategoria
        }).success(function(result){    
            toast.showToast("Categoria atualizada!", "success");
            $s.getCategorias();
        });
    }      

    $s.deleteCategoria = function (oCategoria){

        $s.p = 'deleteCategoria';
        var classe = 'generic';
      
        $http.post(SERVER_PATH + "redirect.php?p=" + $s.p + "&classe=" + classe, {
            oCategoria: oCategoria
        }).success(function(result){    
            toast.showToast("Categoria deletada!", "success");
            $s.getCategorias();
        });
    }  

    // Notas
    $s.insertNota = function (codticket, nova_nota){
        
        $s.p = 'insertNota';
        var classe = 'ticket';

        var oTicket = {};

        oTicket.codusuario = $rs.user.codusuario;
        oTicket.codticket = codticket;
        oTicket.nova_nota = nova_nota;

   
        $http.post(SERVER_PATH + "redirect.php?p=" + $s.p + "&classe=" + classe, {
            oTicket: oTicket
        }).success(function(result){    
            toast.showToast("Nota salva!", "success");            
            $s.nova_nota = '';
            $s.getTicket();            
        });

    }    

}]);


app.controller("userCtrl", ['$scope', '$http', '$rootScope','route','toast','user','date', '$location','user',  function ($s, $http, $rs, route, toast, user, date, $location, user) {

    $s.login_cliente = true;

    $s.setStyleClicado = function(codrecurso){
        $s.recurso_clicado = codrecurso;
    }    


    // INICIO FUNCS CLIENTE LOGIN
    $s.goCadastrarCliente = function(){
        route.goRota('cliente_cadastro');
    }

    $s.getClienteLogin = function(oCliente){

        $s.p = 'getClienteLogin';
        var classe = 'Cliente';
        
        $http.post(SERVER_PATH + "redirect.php?p=" + $s.p + "&classe=" + classe, {
            oCliente: oCliente
        }).success(function(result){            
            if (result.msg != 'false') {
                $rs.user = result;    
                toast.showToast('Bem vindo!', 'success');            
                route.goRota('navbar_completa.tickets_clientes');
            } else {            
                toast.showToast('E-mail inválido!', 'error');
            }
        });
    }

    $s.insertCliente = function(oCliente){
        if (!oCliente.nome || !oCliente.email || !oCliente.tel || !oCliente.cargo) {
            toast.showToast("Preencha todos os campos.", 'warning');
            return;
        }

        user.service_insertCliente(oCliente);

        toast.showToast("Cadastro realizado com sucesso!", "success");
    }

    $s.getClientes = function(){       
        user.service_getClientes();
    }

    // INICIO FUNCS USUARIO
    $s.getUsuarioLogin = function(oUser){

        $s.p = 'getUsuarioLogin';
        var classe = 'Usuario';
        
        $http.post(SERVER_PATH + "redirect.php?p=" + $s.p + "&classe=" + classe, {
            oUser: oUser
        }).success(function(result){            
            if (result.msg != 'false') {
                $rs.user = result;    
                toast.showToast('Bem vindo!', 'success');              
                route.goRota('navbar_completa.tickets');                    
                
            } else {            
                toast.showToast('E-mail ou senha inválidos!', 'error');
            }
        });
    }

    $s.getClienteEditar = function(){
        var p = 'getClienteEditar';
        var classe = 'Cliente';

        $rs.loading = true;

        $http.get(SERVER_PATH + "redirect.php?p=" + p + "&classe=" + classe + "&location=" + $location.path()).success(function(result){            
            $s.cliente_editar = result;   
            $rs.loading = false;
        });
    } 

    $s.updateCliente = function(oCliente){
        user.service_updateCliente(oCliente);        
    }

    $s.getUsuario = function(){    
        var p = 'getUsuario';
        var classe = 'Usuario';

        $http.get(SERVER_PATH + "redirect.php?p=" + p + "&classe=" + classe + "&location=" + $location.path()).success(function(result){            
          $s.usuario_editar = result;         
        });
    }    

    $s.getTodosRecursos = function(){
        user.service_getTodosRecursos();
    }

    $s.updateUsuario = function(oUsuario){
        user.service_updateUsuario(oUsuario);
        route.goRota('navbar_completa.usuarios');
    }

    $s.insertUsuario = function(oUsuario){
        if (oUsuario.nova_senha !== oUsuario.confirmacao_nova_senha) {
            toast.showToast("As senhas digitadas não estão iguais", 'warning');
            return;
        }

        user.service_insertUsuario(oUsuario);
        route.goRota('navbar_completa.usuarios'); 
    }

    $s.recuperarSenha = function(email){
        toast.showToast("E-mail de recuperação enviado para " + email, 'primary', 5000);
    }

    $s.editarUsuario = function(usuario){
        $rs.usuario_editar = usuario;        
    }

    $s.getUsuarios = function(pesquisa){         
        user.service_getUsuarios(pesquisa);        
    }

    $s.destroySession = function(){
        $rs.user = {};
        user.service_destroySession();
    }    
 
}]);


app.controller("configCtrl", ['$scope', '$http', '$rootScope','route','toast','user','date', '$location','empresaProvider', function ($s, $http, $rs, route, toast, user, date, $location, empresaProvider) {

    $s.updateConfiguracoes = function(oConfiguracao){        
        empresaProvider.updateConfiguracoes(oConfiguracao);
        empresaProvider.getConfiguracoes();
        toast.showToast("Configurações salvas!", 'success')
    }

}]);


app.controller("graficosCtrl", ['$scope', '$http', '$rootScope','toast','date', 'graficosProvider', function ($s, $http, $rs, toast, date, graficosProvider) {

    graficosProvider.getGraficoCategorias().then(function(data){

        var ctx = document.getElementById("grafico_categorias").getContext("2d");

        var options = {
            responsive: true,
            legend: {
                position: 'right',
            },
            title: {
                display: true,
                text: 'Tickets por Categoria'
            },
            animation: {
                animateScale: true,
                animateRotate: true
            }
        };

        $s.grafico_categorias = new Chart(ctx,{
            type: 'pie',
            data: data.data.data,
            options: options
        });

    });


    graficosProvider.getGraficoAbertosResolvidosLine().then(function(data){

        var ctx = document.getElementById("grafico_abertos_resolvidos_line").getContext("2d");

        var options = {
            responsive: true,
            legend: {
                position: 'right',
            },
            title: {
                display: true,
                text: 'Abertos VS Resolvidos no mesmo dia'
            },
            tooltips: {
                    mode: 'index',
                    intersect: false,
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },
            animation: {
                animateScale: true,
                animateRotate: true
            }
        };

        $s.grafico_categorias = new Chart(ctx,{
            type: 'line',
            data: data.data.data,
            options: options
        });




        //   $s.teste_funcional = new Chart(ctx,{
        //     type: 'line',
        //     data: {
        //     labels: [1500,1600,1700,1750,1800,1850,1900,1950,1999,2050],
        //     datasets: [{ 
        //         data: [86,114,106,106,107,111,133,221,783,2478],
        //         label: "Africa",
        //         borderColor: "#3e95cd",
        //         fill: false
        //       }, { 
        //         data: [282,350,411,502,635,809,947,1402,3700,5267],
        //         label: "Asia",
        //         borderColor: "#8e5ea2",
        //         fill: false
        //       }, { 
        //         data: [168,170,178,190,203,276,408,547,675,734],
        //         label: "Europe",
        //         borderColor: "#3cba9f",
        //         fill: false
        //       }, { 
        //         data: [40,20,10,16,24,38,74,167,508,784],
        //         label: "Latin America",
        //         borderColor: "#e8c3b9",
        //         fill: false
        //       }, { 
        //         data: [6,3,2,2,7,26,82,172,312,433],
        //         label: "North America",
        //         borderColor: "#c45850",
        //         fill: false
        //       }
        //     ]
        //   },
        //     options: options
        // });

        console.log(data.data.data);



        // console.log($s.teste_funcional.data);


    });


}]);